package com.company;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        double pizza_price = 0;
        String ordered_pizza = "";
        String top_displayed = ""; //will be displayed to the costumer when they finish ordering
        String pizza_size_displayed = "" ; //will be displayed to the costumer when they finish ordering
        System.out.println("Welcome to tasty pizza restaurant..."); //assuming that our restaurant is called tasty pizza
        System.out.println("Please choose your pizza from the menu below, type only the pizza number:");
        System.out.println("1. pizza one     10dkk       2. pizza two     20dkk ");
        System.out.println("3. pizza three   30dkk       4. pizza four    40dkk");
        System.out.println("5. pizza five    50dkk       6. pizza six     60dkk");
        System.out.println("7. pizza seven   70dkk       8. pizza eight   80dkk");
        System.out.println("9. pizza nine    90dkk       10. pizza ten    100dkk");
        /*
        in order to make things easier when testing
        I set prices that correspond to the pizza name/number
         */
        System.out.println("I want pizza number: ");
        Scanner order = new Scanner(System.in);
        int pizza = order.nextInt();
        /*
        receiving the pizza number from the costumer
        and setting the price accordingly
         */
        switch (pizza) {
            case 1:
                ordered_pizza = "pizza one";
                pizza_price = 10.;
                break;
            case 2:
                ordered_pizza = "pizza two";
                pizza_price = 20.;
                break;
            case 3:
                ordered_pizza = "pizza three";
                pizza_price = 30.;
                break;
            case 4:
                ordered_pizza = "pizza four";
                pizza_price = 40.;
                break;
            case 5:
                ordered_pizza = "pizza five";
                pizza_price = 50.;
                break;
            case 6:
                ordered_pizza = "pizza six";
                pizza_price = 60.;
                break;
            case 7:
                ordered_pizza = "pizza seven";
                pizza_price = 70.;
                break;
            case 8:
                ordered_pizza = "pizza eight";
                pizza_price = 80.;
                break;
            case 9:
                ordered_pizza = "pizza nine";
                pizza_price = 90.;
                break;
            case 10:
                ordered_pizza = "pizza ten";
                pizza_price = 100.;
                break;
        }
        /*
        asking for the size and adjusting the price accordingly
         */
        System.out.println("What size do you want: ");
        System.out.println("1. Child       2. Standard       3. Family ");
        int pizza_size = order.nextInt();
        switch (pizza_size) {
            case 1:
                pizza_price = pizza_price * 0.75;
                pizza_size_displayed = "Child size";
                break;
            case 2:
            default:
                pizza_price = pizza_price;
                pizza_size_displayed = "Standard size";
                break;
                /* if the user enters any value other 1, 2, or 3 it will be
                by default considered as a standard size(2)
                */
            case 3:
                pizza_price = pizza_price * 1.5;
                pizza_size_displayed = "Family size";
                break;
        }

        boolean top = false;
        /*
        asking the user whether they want any topping, if not they can press 0.
        and adjusting the price accordingly.
        if they enter any value other than 0,1,2,3 or 4 it will by default considered
        as 0 (no more topping).
         */
        System.out.println("Would you like any topping?");
        System.out.println("1. topping 1     2. topping 2");
        System.out.println("3. topping 3     4. topping 4");
        while (!top) {
            System.out.println("Please enter the topping number then 0 to continue");
            System.out.println("Please enter 0 if you don't want any topping");
            int topping = order.nextInt();
            if (topping == 0) {
                top = true;
            } else if (topping == 1) {
                pizza_price += 5;
                top_displayed += " topping one";
                top = false;
            } else if (topping == 2) {
                pizza_price += 5;
                top_displayed += ", topping two";
                top = false;
            } else if (topping == 3) {
                pizza_price += 5;
                top_displayed += ", topping three";
                top = false;
            } else if (topping == 4) {
                pizza_price += 5;
                top_displayed += ", topping four";
                top = false;
            } else {
                pizza_price = pizza_price;
                top = true;
            }
        }
        if (top_displayed != "") { // if they want topping:
            System.out.println("You have ordered: " + ordered_pizza + ", " + pizza_size_displayed + " with: " + top_displayed);
        } else {// if they don't want any topping:
            System.out.println("You have ordered: " + ordered_pizza + ", " + pizza_size_displayed + " with no topping ");
        }
        System.out.println("The total price is: "+pizza_price + "dkk");
    }
}

